﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace PieloJPWP
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly List<string> _categories = new List<string>{"Building","Plane", "Car", "Ship", "Garden","Dog"};
        private List<string> _addedPictures = new List<string>();


        private static int _numberOfPicturesInFolders = 7;
        private int _findCategory = 0;

        private DispatcherTimer _dispatcherTimer;

        private int _counter = 60;

        public MainWindow()
        {
            InitializeComponent();
            
        }

        void StartGame()
        {
            StartTimer();
            // get random category
            Random r = new Random(DateTime.Now.Millisecond);
            int category = r.Next(0, _categories.Count);

            _findCategory = category;

            CategoryToFind.Content = "Category to find : " + _categories.ElementAt(category);

            DrawGameImages();

            
        }

        private void StartTimer()
        {
            _dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            _dispatcherTimer.Tick += dispatcherTimer_Tick;
            _dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            _dispatcherTimer.Start();
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            _counter--;
            if (_counter == 0)
                _dispatcherTimer.Stop();
            TimerLabel.Content =  "Time : " + _counter.ToString();
        }

        void DrawGameImages()
        {
            List<string> positions = new List<string>{"10","11","12","20","21","22","30", "31", "32", };

            Random r = new Random(DateTime.Now.Millisecond);

            for (int i = 0; i < 8; i++)
            {
                int category;
                do
                {
                    category = r.Next(0, _categories.Count);
                } while (category == _findCategory);

                Image image = GetImageFromFolder(category);
                image.MouseDown += new MouseButtonEventHandler(WrongImageClick);

                var posOfImgInList = r.Next(0, positions.Count);
                var posOfImg = positions[posOfImgInList];

                Grid.SetRow(image,(int)(posOfImg[0] - '0'));
                Grid.SetColumn(image, (int)(posOfImg[1] - '0'));

                GameGrid.Children.Add(image);

                positions.Remove(positions.ElementAt(posOfImgInList));
            }
            var posOfMainImg = positions.First();
            Image mainImage = GetImageFromFolder(_findCategory);
            mainImage.MouseDown += new MouseButtonEventHandler(MainImageClick);
            Grid.SetRow(mainImage, (int)(posOfMainImg[0] - '0'));
            Grid.SetColumn(mainImage, (int)(posOfMainImg[1] - '0'));
            GameGrid.Children.Add(mainImage);

        }

        private void MainImageClick(object sender, MouseButtonEventArgs e)
        {
            MessageBox.Show("Yea! Found it!");
            BackToMenu();
        }

        private void WrongImageClick(object sender, MouseButtonEventArgs e)
        {
            MessageBox.Show("Oooopppsss, thats not : " + _categories.ElementAt(_findCategory));
        }

        private Image GetImageFromFolder(int category)
        {
            var categoryFolder = _categories.ElementAt(category).ToLower();

            Random r = new Random(DateTime.Now.Millisecond);
            int imageNumber;
            do
            {
                imageNumber = r.Next(1, _numberOfPicturesInFolders + 1);
            } while (_addedPictures.Contains(categoryFolder+imageNumber));
            

            var uri = new Uri("pack://application:,,,/Images/" + categoryFolder + 's' + "/" + categoryFolder + imageNumber + ".jpg");

            Image image = new Image();
            BitmapImage src = new BitmapImage();
            src.BeginInit();
            src.UriSource = uri;
            src.CacheOption = BitmapCacheOption.OnLoad;
            src.EndInit();
            image.Source = src;
            image.Stretch = Stretch.None;
            return image;
        }

        private void StartButtonClick(object sender, RoutedEventArgs e)
        {
            StartButton.Visibility = Visibility.Hidden;
            Title.Visibility = Visibility.Hidden;
            QuitButton.Visibility = Visibility.Hidden;
            MenuGrid.Visibility = Visibility.Visible;

            StartGame();
        }

        private void BackToMenu()
        {
            StartButton.Visibility = Visibility.Visible;
            Title.Visibility = Visibility.Visible;
            QuitButton.Visibility = Visibility.Visible;
            MenuGrid.Visibility = Visibility.Hidden;
        }

        private void QuitButton_OnClickButtonClick(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void EndGameButton_OnClick(object sender, RoutedEventArgs e)
        {
            BackToMenu();
        }
    }
}
